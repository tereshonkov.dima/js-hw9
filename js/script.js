"use strict"
// 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.

// Використайте 2 способи для пошуку елементів.

// Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).

const featureAllElements = document.querySelectorAll(".feature");
console.log(featureAllElements);
const featureAllElementsTwo = document.getElementsByClassName("feature");
console.log(featureAllElementsTwo);

// featureAllElementsTwo[0].style.textAlign = "center";
// featureAllElementsTwo[1].style.textAlign = "center";


for (const elem of featureAllElements) {
    elem.style.textAlign = "center";
}

// 2. Змініть текст усіх елементів h2 на "Awesome feature".

const header = document.getElementsByTagName("h2");

// header[0].textContent = "Awesome feature";

for (const elem of header) {
    elem.textContent = "Awesome feature";
}

// 3.Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".

const allElem = document.querySelectorAll(".feature-title");

for (const elem of allElem) {
    elem.textContent += "!";
}


